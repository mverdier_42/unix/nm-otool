# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/14 14:55:01 by mverdier          #+#    #+#              #
#    Updated: 2018/11/06 18:12:37 by mverdier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Colors.

ORANGE =	\033[1;33m   #It is actually Yellow, but i changed yellow to orange.

GREEN =		\033[1;32m

RED =		\033[1;31m

RES =		\033[0m

#------------------------------------------------------------------------------#

NM =		./nm

OTOOL =		./otool

LIBDIR =	./libft

all: nm otool

nm:
	@$(MAKE) -C $(NM)

otool:
	@$(MAKE) -C $(OTOOL)

clean:
	@$(MAKE) -C $(NM) clean
	@$(MAKE) -C $(OTOOL) clean

fclean:
	@$(MAKE) -C $(NM) fclean
	@$(MAKE) -C $(OTOOL) fclean

re: fclean
	@$(MAKE) all

#------------------------------------------------------------------------------#
# List of all my optionnals but usefull rules.

norm:
	@$(MAKE) -C $(NM) norm
	@$(MAKE) -C $(OTOOL) norm

# A rule to make git add easier

git:
	@$(MAKE) -C $(LIBDIR) git
	@$(MAKE) -C $(NM) git
	@$(MAKE) -C $(OTOOL) git
	git add Makefile
	git status

.PHONY: all clean re fclean git norm check nm otool
