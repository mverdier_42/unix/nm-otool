/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_size_flags.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 16:01:46 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 16:01:54 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_size.h"

int			size_flags_part1(t_params *param, t_args *tmp, int size)
{
	if (param->hash && (((param->type == 'o' || param->type == 'O')
					&& !arg_is_zero(tmp))))
		size++;
	else if (param->hash && (((param->type == 'x' || param->type == 'X')
					&& !arg_is_zero(tmp)) || param->type == 'p'))
		size += 2;
	param->len = size;
	if ((param->plus || param->space) && arg_is_positive(tmp)
			&& arg_is_int(tmp))
		size++;
	if ((param->plus || param->space) && arg_is_positive(tmp)
			&& arg_is_int(tmp) && param->minus && !param->prec)
		(param->len)--;
	if (param->type == 'p' && !param->prec && param->is_prec
			&& arg_is_zero(tmp))
		size--;
	if (param->type == 'p' && param->prec > 0)
		size += ((size - 2 < param->prec) ? (param->prec - (size - 2)) : 0);
	return (size);
}

static int	part2_prec(t_params *param, t_args *tmp, int size)
{
	if (!arg_is_char(tmp))
	{
		if (size < param->prec)
		{
			size += ((size < param->prec) ? (param->prec - size) : 0);
			if ((param->plus || param->space) && arg_is_positive(tmp)
					&& arg_is_int(tmp) && size > param->len)
				size++;
		}
		if (size == param->prec && !arg_is_zero(tmp) && param->hash &&
				(param->type == 'x' || param->type == 'X'))
		{
			param->len -= 2;
			size += 2;
		}
	}
	return (size);
}

int			size_flags_part2(t_params *param, t_args *tmp, int size)
{
	if (!param->prec && arg_is_zero(tmp) == 1 && param->is_prec
			&& (!param->hash || (param->hash && (param->type == 'x'
						|| param->type == 'X'))))
	{
		size -= param->len;
		param->len = 0;
	}
	if (!arg_is_positive(tmp) && param->plus)
		param->len -= 1;
	if (param->prec > param->len && (param->plus || param->space))
		size += 1;
	size += ((size < param->width) ? (param->width - size) : 0);
	size = part2_prec(param, tmp, size);
	return (size);
}

int			size_types(t_params *param, t_args **args, va_list ap)
{
	int size;

	size = 0;
	if (param->type == 'd' || param->type == 'i' || param->type == 'D')
		size = calc_int(param, ap, args);
	else if (param->type == 'u' || param->type == 'U')
		size = calc_unsigned(param, ap, args);
	else if (param->type == 'o' || param->type == 'O')
		size = calc_octal(param, ap, args);
	else if (param->type == 'x' || param->type == 'X')
		size = calc_hexa(param, ap, args);
	else if (param->type == 'c' || param->type == 'C' || param->type == '%')
		size = calc_char(param, ap, args);
	else if (param->type == 's' || param->type == 'S')
		size = calc_string(param, ap, args);
	else if (param->type == 'p')
		size = calc_ptr(param, ap, args);
	else if (param->type == 'b')
		size = calc_bin(param, ap, args);
	return (size);
}
