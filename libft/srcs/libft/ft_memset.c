/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:45:58 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/03 21:08:37 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	*cpy;

	cpy = (unsigned char *)b;
	while (len > 0)
	{
		*cpy = (unsigned char)c;
		cpy++;
		len--;
	}
	return (b);
}
