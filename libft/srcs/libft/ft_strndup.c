/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/06 14:14:59 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/06 14:29:25 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

char	*ft_strndup(const char *s, int len)
{
	char	*d;
	int		n;

	if ((d = (char *)malloc(sizeof(*d) * len + 1)) == NULL)
		return (NULL);
	n = 0;
	while (s[n] != '\0' && n < len)
	{
		d[n] = s[n];
		n++;
	}
	d[n] = '\0';
	return (d);
}
