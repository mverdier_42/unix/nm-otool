/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrextract.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 13:24:51 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/09 13:24:53 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_strrextract(char *str, char c)
{
	char	*tmp;

	if (!(tmp = ft_strrchr(str, c)))
		return (ft_strdup(str));
	return (ft_strsub(str, 0, tmp - str));
}
