/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sortinttab.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/06 12:34:24 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/24 22:07:31 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		*ft_sortinttab(int *tab, int len)
{
	int	i;

	i = 0;
	while (i + 1 < len)
	{
		if (tab[i] > tab[i + 1])
		{
			ft_intswap(&(tab[i]), &(tab[i + 1]));
			if (i >= 1)
				i -= 2;
			else
				i--;
		}
		i++;
	}
	return (tab);
}
