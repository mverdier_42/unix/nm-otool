/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlst_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 14:24:21 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/11 20:18:48 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void	ft_dlst_push_back(t_dlist_b **dlist_b, t_dlist *new)
{
	if (!(*dlist_b)->first)
	{
		new->prev = new;
		new->next = new;
		(*dlist_b)->first = new;
		(*dlist_b)->last = new;
		(*dlist_b)->size++;
		return ;
	}
	new->prev = (*dlist_b)->last;
	new->next = (*dlist_b)->first;
	(*dlist_b)->last->next = new;
	(*dlist_b)->first->prev = new;
	if ((*dlist_b)->first == (*dlist_b)->last)
		(*dlist_b)->first->next = new;
	(*dlist_b)->last = new;
	(*dlist_b)->size++;
}
