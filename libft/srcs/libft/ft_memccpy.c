/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:44:27 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/03 21:21:02 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char	*d;

	d = dest;
	while (n > 0)
	{
		if (*(char *)src == c)
		{
			*d = *(char *)src;
			d++;
			return (d);
		}
		*d = *(char *)src;
		d++;
		src++;
		n--;
	}
	return (NULL);
}
