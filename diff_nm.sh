#!/bin/zsh
for file in $@ #/bin/* /sbin/* /usr/bin/* /usr/sbin/* /usr/lib/* /usr/lib/*/* /usr/libexec/* /usr/libexec/*/*
do
	echo "diff on $file"
	nm $file > resnm 2> errnm
	~/Desktop/nm-otool/ft_nm $file > resftnm 2> errftnm
	diff resnm resftnm
done
