/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_header32.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/04 19:52:04 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/30 16:28:13 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

struct mach_header	fill_header32(void *ptr, t_header file)
{
	struct mach_header	*src;
	struct mach_header	des;

	if ((size_t)file.size < sizeof(struct mach_header))
	{
		des.magic = 0;
		ft_dprintf(2, "%s: unaccessible data (mach_header)\n", file.name);
		return (des);
	}
	src = (struct mach_header*)ptr;
	des.magic = swap(src->magic, file);
	des.filetype = swap(src->filetype, file);
	des.ncmds = swap(src->ncmds, file);
	des.sizeofcmds = swap(src->sizeofcmds, file);
	des.flags = swap(src->flags, file);
	return (des);
}
