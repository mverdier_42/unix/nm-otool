/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_symbols64.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 18:14:43 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/06 19:14:35 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

bool	print_symbols64(t_symbol *symbols, int ac, t_header infos)
{
	t_symbol	*sym;

	if (!symbols)
		return (false);
	sym = symbols;
	if (ac > 2 && !infos.is_fat)
		ft_printf("\n%s:\n", infos.name);
	while (sym)
	{
		if (!(infos.opt & OPT_J) && !(infos.opt & OPT_ULOW) &&
			sym->value == -1)
			ft_printf("%16c %c ", ' ', sym->symbol);
		else if (!(infos.opt & OPT_J) && !(infos.opt & OPT_ULOW))
			ft_printf("%.16llx %c ", sym->value, sym->symbol);
		put_nm_str(sym->name, sym, infos);
		sym = sym->next;
	}
	delete_symbols(symbols);
	return (true);
}
