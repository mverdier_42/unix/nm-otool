/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_fat_arch.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 16:53:46 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/29 16:55:13 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static void		fill_des(struct fat_arch *src, struct fat_arch *des,
	t_header file)
{
	des->offset = swap(src->offset, file);
	des->size = swap(src->size, file);
	des->align = swap(src->align, file);
	des->cputype = swap(src->cputype, file);
}

struct fat_arch	fill_fat_arch(void *ptr, t_header file, off_t offset)
{
	struct fat_arch	*src;
	struct fat_arch	des;

	if (file.size < offset + (off_t)sizeof(struct fat_arch))
	{
		des.offset = 0;
		ft_dprintf(2, "%s: unaccessible data (fat_arch)\n", file.name);
		return (des);
	}
	src = (struct fat_arch*)ptr;
	fill_des(src, &des, file);
	if (des.offset + des.size > file.size)
	{
		ft_dprintf(2, "%s: fat arch offset + size extends file size\n",
			file.name);
		des.offset = 0;
	}
	if (des.align > 15)
	{
		ft_dprintf(2, "%s: fat arch align(2^%u) is too large, max is 2^15\n",
			file.name, des.align);
		des.offset = 0;
	}
	return (des);
}
