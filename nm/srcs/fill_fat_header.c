/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_fat_header.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 15:58:32 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/30 16:27:57 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

struct fat_header	fill_fat_header(void *ptr, t_header file)
{
	struct fat_header	*src;
	struct fat_header	des;

	if ((size_t)file.size < sizeof(struct fat_header))
	{
		des.magic = 0;
		ft_dprintf(2, "%s: unaccessible data (fat_header)\n", file.name);
		return (des);
	}
	src = (struct fat_header*)ptr;
	des.magic = swap(src->magic, file);
	des.nfat_arch = swap(src->nfat_arch, file);
	return (des);
}
