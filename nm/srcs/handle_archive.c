/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_archive.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 15:08:55 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/06 17:59:26 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static void	dump(void *ptr, t_header *obj, t_header infos)
{
	obj->opt = infos.opt;
	if (obj->is_fat)
		dump_fat_header(ptr, *obj, 2);
	else if (obj->is_64)
		print_symbols64(dump_header64(ptr, *obj), 2, infos);
	else
		print_symbols32(dump_header32(ptr, *obj), 2, infos);
}

bool		handle_archive(void *ptr, t_header infos)
{
	size_t			offset;
	size_t			len;
	struct ar_hdr	*header;
	char			*str;
	t_header		obj;

	header = (struct ar_hdr*)(ptr + SARMAG);
	offset = SARMAG + sizeof(*header) + ft_atoi(header->ar_size);
	while ((off_t)offset < infos.size)
	{
		header = (struct ar_hdr*)(ptr + offset);
		if (ft_atoi(header->ar_size) <= 0)
			return (err("%s: bad archive size\n", infos.name));
		str = ptr + offset + sizeof(*header);
		len = offset + sizeof(*header) + ft_atoi(header->ar_name + 3);
		if (offset + sizeof(*header) + ft_atoi(header->ar_size) >
			(size_t)infos.size)
			return (err("%s: archive extends past end of file\n", infos.name));
		ft_printf("\n%s(%s):\n", infos.name, str);
		if (!get_header(ptr + len, &obj, infos.size, str))
			return (false);
		dump(ptr + len, &obj, infos);
		offset += sizeof(*header) + ft_atoi(header->ar_size);
	}
	return (true);
}
