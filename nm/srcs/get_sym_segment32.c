/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_sym_segment32.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:02:07 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/21 14:35:01 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static char		get_sectname(struct section *sect, t_seg *seg, uint32_t i)
{
	if (seg->nsect == i + seg->count + 1)
	{
		if (!ft_strcmp(sect->sectname, "__text"))
			return ('t');
		else if (!ft_strcmp(sect->sectname, "__data"))
			return ('d');
		else if (!ft_strcmp(sect->sectname, "__bss"))
			return ('b');
		else if (!ft_strcmp(sect->sectname, "__common"))
			return ('s');
		else
			return ('s');
	}
	return ('.');
}

static uint32_t	get_sect(void *ptr, struct load_command *lc,
	t_seg *seg, t_header infos)
{
	struct segment_command	*sc;

	if ((size_t)((void*)lc - ptr) + sizeof(*sc) > (size_t)infos.size)
	{
		ft_dprintf(2, "%s: unaccessible data (segment_command)\n",
			infos.name);
		return (-2);
	}
	sc = (struct segment_command*)lc;
	if (seg->nsect > (swap(sc->nsects, infos) + seg->count))
	{
		seg->count += swap(sc->nsects, infos);
		return (-1);
	}
	if ((size_t)((void*)sc - ptr) + sizeof(*sc) + sizeof(struct section) >
		(size_t)infos.size)
	{
		ft_dprintf(2, "%s: unaccessible data (segment_command)\n",
			infos.name);
		return (-2);
	}
	return (swap(sc->nsects, infos));
}

static char		get_sym_section32(t_header infos, t_seg *seg,
	struct load_command *lc, void *ptr)
{
	struct section	*sect;
	uint32_t		i;
	uint32_t		nsects;
	char			c;

	i = 0;
	if ((nsects = get_sect(ptr, lc, seg, infos)) == (uint32_t)-1)
		return ('.');
	if (nsects == (uint32_t)-2)
		return ('?');
	sect = (struct section*)((void*)lc + sizeof(struct segment_command));
	while (i < nsects)
	{
		if ((c = get_sectname(sect, seg, i)) != '.')
			return (c);
		if ((size_t)((void*)sect - ptr) + 2 * sizeof(*sect) >
			(size_t)infos.size)
		{
			ft_dprintf(2, "%s: unaccessible data (section)\n", infos.name);
			return ('?');
		}
		sect = (void*)sect + sizeof(*sect);
		i++;
	}
	return ('.');
}

static char		get_lc_seg(void *ptr, struct load_command **lc,
	t_seg *seg, t_header infos)
{
	char	c;

	if (swap((*lc)->cmd, infos) == LC_SEGMENT)
	{
		if ((c = get_sym_section32(infos, seg, *lc, ptr)) != '.')
			return (c);
	}
	if ((size_t)((void*)*lc - ptr) + swap((*lc)->cmdsize, infos) +
		sizeof(**lc) > (size_t)infos.size)
	{
		ft_dprintf(2, "%s: unaccessible data (load_command)\n", infos.name);
		return ('?');
	}
	*lc = (void*)*lc + swap((*lc)->cmdsize, infos);
	return ('.');
}

char			get_sym_segment32(void *ptr, t_header infos,
	uint8_t nsect)
{
	struct mach_header		header;
	struct load_command		*lc;
	uint32_t				i;
	t_seg					seg;
	char					c;

	header = fill_header32(ptr, infos);
	if (header.magic == 0)
		return ('?');
	if (nsect == NO_SECT)
		return ('s');
	if (sizeof(header) + sizeof(*lc) > (size_t)infos.size)
	{
		ft_dprintf(2, "%s: unaccessible data (load_command)\n", infos.name);
		return ('?');
	}
	i = 0;
	seg.count = 0;
	seg.nsect = nsect;
	lc = ptr + sizeof(header);
	while (i++ < header.ncmds)
		if ((c = get_lc_seg(ptr, &lc, &seg, infos)) != '.')
			return (c);
	return ('s');
}
