/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_symbols.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 20:48:55 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/06 19:14:52 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

void	delete_symbols(t_symbol *symbols)
{
	t_symbol	*tmp;

	while (symbols != NULL)
	{
		tmp = symbols->next;
		free(symbols);
		symbols = tmp;
	}
}
