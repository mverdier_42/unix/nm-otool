/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_fat_header.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 13:43:20 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/09 16:28:14 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

void	swap_fat_header(struct fat_header *header)
{
	header->magic = 0;
	header->magic = swap_uint32(header->magic);
	header->nfat_arch = swap_uint32(header->nfat_arch);
}
