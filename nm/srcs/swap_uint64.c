/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_uint64.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 13:26:29 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/02 13:29:58 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

uint64_t	swap_uint64(uint64_t val)
{
	return (((val >> 56) & 0x00000000000000FF) |
	((val >> 40) & 0x000000000000FF00) | ((val >> 24) & 0x0000000000FF0000) |
	((val >> 8) & 0x00000000FF000000) | ((val << 8) & 0x000000FF00000000) |
	((val << 24) & 0x0000FF0000000000) | ((val << 40) & 0x00FF000000000000) |
	((val << 56) & 0xFF00000000000000));
}
