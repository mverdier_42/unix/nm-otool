/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dump_header64.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 17:38:48 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/06 19:18:49 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static bool	get_symtab(void *ptr, t_header infos, t_symbol **symbols,
	struct mach_header_64 header)
{
	struct load_command		*lc;
	uint32_t				i;

	i = 0;
	if (infos.size - sizeof(header) < sizeof(*lc))
		return (err("%s: unaccessible data (load_command)\n", infos.name));
	lc = ptr + sizeof(header);
	while (i < header.ncmds)
	{
		if (swap(lc->cmd, infos) == LC_SYMTAB)
			if (!get_symbol64(ptr, lc, infos, symbols))
				return (false);
		if ((size_t)((void*)lc - ptr) + swap(lc->cmdsize, infos) + sizeof(*lc) >
			(size_t)infos.size)
			return (err("%s: unaccessible data (load_command)\n", infos.name));
		lc = (void*)lc + swap(lc->cmdsize, infos);
		i++;
	}
	return (true);
}

t_symbol	*dump_header64(void *ptr, t_header infos)
{
	struct mach_header_64	header;
	t_symbol				*symbols;

	symbols = NULL;
	header = fill_header64(ptr, infos);
	if (header.magic == 0)
		return (NULL);
	if (!is_valid_file(header.filetype))
	{
		ft_dprintf(2, "%s was not recognized as a valid object file.\n",
			infos.name);
		return (NULL);
	}
	if (!get_symtab(ptr, infos, &symbols, header))
	{
		delete_symbols(symbols);
		return (NULL);
	}
	return (symbols);
}
