/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 13:58:55 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/06 19:22:32 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

int		main(int ac, char **av)
{
	int			i;
	int			n;
	t_header	infos;

	if ((i = parse_options(ac, av, &infos)) == 0)
		return (0);
	if (ac < 2 || i == ac)
	{
		ft_nm("a.out", ac, infos);
		return (0);
	}
	n = ac - i + 1;
	while (i < ac)
	{
		ft_nm(av[i], n, infos);
		i++;
	}
	return (0);
}
