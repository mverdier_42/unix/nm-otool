/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_symbol32.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 17:34:06 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/29 17:03:40 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static char	get_symbol_sym32(void *ptr, struct nlist nlist, t_header infos)
{
	if ((nlist.n_type & N_STAB))
		return ('.');
	else if ((nlist.n_type & N_TYPE) == N_UNDF)
		return (get_symcase(nlist.n_type, 'u'));
	else if ((nlist.n_type & N_TYPE) == N_ABS)
		return (get_symcase(nlist.n_type, 'a'));
	else if ((nlist.n_type & N_TYPE) == N_INDR)
		return (get_symcase(nlist.n_type, 'i'));
	else if ((nlist.n_type & N_TYPE) == N_SECT)
	{
		return (get_symcase(nlist.n_type,
			get_sym_segment32(ptr, infos, nlist.n_sect)));
	}
	return ('.');
}

static bool	init_garb(t_garb32 *garb, void *ptr, struct load_command *lc,
	t_header infos)
{
	if ((size_t)((void*)lc - ptr) + sizeof(*garb->st_cmd) > (size_t)infos.size)
		return (err("%s: unaccessible data (symtab_command)\n", infos.name));
	garb->st_cmd = (struct symtab_command*)lc;
	if (swap(garb->st_cmd->symoff, infos) >= infos.size)
		return (err("%s: unaccessible data (nlist)\n", infos.name));
	garb->array = (struct nlist*)(ptr + swap(garb->st_cmd->symoff, infos));
	garb->tab = ptr + swap(garb->st_cmd->stroff, infos);
	garb->i = 0;
	return (true);
}

static int	get_symbol_sym_val32(t_garb32 *garb, void *ptr, t_header infos)
{
	if ((size_t)((void*)garb->array - ptr) +
		(garb->i + 1) * sizeof(*garb->array) > (size_t)infos.size)
		return (err("%s: unaccessible data (nlist)\n", infos.name));
	if ((garb->symbol = malloc(sizeof(t_symbol))) == NULL)
		return (0);
	if (swap(garb->array[garb->i].n_sect, infos) != NO_SECT ||
		((garb->array[garb->i].n_type & N_TYPE) == N_ABS) ||
		((garb->array[garb->i].n_type & N_TYPE) == N_INDR))
		garb->symbol->value = swap(garb->array[garb->i].n_value, infos);
	else
		garb->symbol->value = -1;
	garb->symbol->symbol = get_symbol_sym32(ptr, garb->array[garb->i], infos);
	if (garb->symbol->symbol == '.' || garb->symbol->symbol == '?')
	{
		free(garb->symbol);
		if (garb->symbol->symbol == '?')
			return (0);
		garb->i++;
		return (1);
	}
	return (2);
}

bool		get_symbol32(void *ptr, struct load_command *lc, t_header infos,
		t_symbol **symbols)
{
	t_garb32	garb;
	int			res;

	if (!init_garb(&garb, ptr, lc, infos))
		return (false);
	while (garb.i < swap(garb.st_cmd->nsyms, infos))
	{
		if ((res = get_symbol_sym_val32(&garb, ptr, infos)) == 0)
			return (false);
		else if (res == 1)
			continue ;
		if (garb.tab + swap(garb.array[garb.i].n_un.n_strx, infos) >=
			garb.tab + swap(garb.st_cmd->strsize, infos))
		{
			free(garb.symbol);
			return (err("%s: bad string index\n", infos.name));
		}
		garb.symbol->name_off = swap(garb.array[garb.i++].n_un.n_strx, infos);
		garb.symbol->name = garb.tab + garb.symbol->name_off;
		garb.symbol->str_off = swap(garb.st_cmd->stroff, infos);
		garb.symbol->str_size = swap(garb.st_cmd->strsize, infos);
		if (!insert_symbol(symbols, garb.symbol, infos))
			free(garb.symbol);
	}
	return (true);
}
