/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_options.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 15:03:25 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/19 17:13:46 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static int	is_opt(int c)
{
	if (ft_strchr("gnpruUj", c) != NULL)
		return (1);
	ft_printf("-%c is not recognized as a valid option.\n", c);
	return (0);
}

static int	get_opt_val(int c)
{
	int				i;
	int				size;
	static t_opt	options[] = { {OPT_G, 'g'}, {OPT_N, 'n'}, {OPT_P, 'p'},
		{OPT_R, 'r'}, {OPT_ULOW, 'u'}, {OPT_UUP, 'U'}, {OPT_J, 'j'} };

	i = 0;
	size = sizeof(options) / sizeof(t_opt);
	while (i < size)
	{
		if (c == options[i].c)
			return (options[i].val);
		i++;
	}
	return (0);
}

static bool	get_option(char *str, t_header *infos)
{
	int		i;
	int		len;
	int		val;

	i = 0;
	len = ft_strlen(str);
	while (i < len)
	{
		val = get_opt_val(str[i]);
		if (infos->opt & val)
		{
			ft_printf("-%c may only occur zero or one times\n");
			return (false);
		}
		infos->opt += val;
		i++;
	}
	return (true);
}

int			parse_options(int ac, char **av, t_header *infos)
{
	int				i;

	i = 1;
	infos->opt = OPT_NONE;
	while (i < ac && av[i][0] == '-')
	{
		if (!ft_str_test_chars(av[i] + 1, is_opt))
			return (0);
		if (!get_option(av[i] + 1, infos))
			return (0);
		i++;
	}
	return (i);
}
