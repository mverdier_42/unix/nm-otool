/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_header64.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/04 19:52:04 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/30 16:28:24 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

struct mach_header_64	fill_header64(void *ptr, t_header file)
{
	struct mach_header_64	*src;
	struct mach_header_64	des;

	if ((size_t)file.size < sizeof(struct mach_header_64))
	{
		des.magic = 0;
		ft_dprintf(2, "%s: unaccessible data (mach_header_64)\n", file.name);
		return (des);
	}
	src = (struct mach_header_64*)ptr;
	des.magic = swap(src->magic, file);
	des.filetype = swap(src->filetype, file);
	des.ncmds = swap(src->ncmds, file);
	des.sizeofcmds = swap(src->sizeofcmds, file);
	des.flags = swap(src->flags, file);
	return (des);
}
