/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_nm_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 17:26:41 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/28 13:37:42 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

void	put_nm_str(char *str, t_symbol *sym, t_header infos)
{
	int		offset;
	int		len;

	offset = sym->str_off + sym->name_off + 1;
	len = 0;
	while (*(str + len) && offset + len < infos.size)
		len++;
	write(1, str, len);
	ft_putchar('\n');
	if (sym->name_off + len > sym->str_size)
		ft_putchar('\n');
}
