/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert_symbol.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 20:24:55 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/27 16:31:37 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

static bool	check_sym(t_symbol *sym, t_header infos)
{
	if ((infos.opt & OPT_G) && !ft_isupper(sym->symbol))
		return (false);
	if ((infos.opt & OPT_ULOW) && ft_tolower(sym->symbol) != 'u')
		return (false);
	if ((infos.opt & OPT_UUP) && ft_tolower(sym->symbol) == 'u')
		return (false);
	return (true);
}

static bool	sort(t_symbol *sym, t_symbol *tmp)
{
	int		cmp;

	cmp = ft_strcmp(sym->name, tmp->name);
	if (cmp < 0 || (!cmp && sym->value < tmp->value))
		return (true);
	return (false);
}

static bool	sym_cmp(t_symbol *sym, t_symbol *tmp, t_header infos)
{
	long long int	cmp;

	cmp = sym->value - tmp->value;
	if ((infos.opt & OPT_P))
		return ((infos.opt & OPT_R));
	else if ((infos.opt & OPT_N) && (infos.opt & OPT_R))
		return (cmp > 0 || (!cmp && !sort(sym, tmp)));
	else if ((infos.opt & OPT_N) && !(infos.opt & OPT_R))
		return (cmp < 0 || (!cmp && sort(sym, tmp)));
	else if ((infos.opt & OPT_R) && !sort(sym, tmp))
		return (true);
	else if (!(infos.opt & OPT_R) && sort(sym, tmp))
		return (true);
	return (false);
}

static bool	insert_sym(t_symbol **symbols, t_symbol *symbol, t_header infos)
{
	t_symbol	*tmp;
	t_symbol	*tmp_prev;

	tmp = *symbols;
	tmp_prev = NULL;
	while (tmp)
	{
		if (!check_sym(symbol, infos))
			return (false);
		if (sym_cmp(symbol, tmp, infos))
		{
			symbol->next = tmp;
			if (tmp_prev != NULL)
				tmp_prev->next = symbol;
			else
				*symbols = symbol;
			return (true);
		}
		tmp_prev = tmp;
		tmp = tmp->next;
	}
	tmp_prev->next = symbol;
	return (true);
}

bool		insert_symbol(t_symbol **symbols, t_symbol *symbol, t_header infos)
{
	symbol->next = NULL;
	if (*symbols == NULL)
	{
		if (!check_sym(symbol, infos))
			return (false);
		*symbols = symbol;
		return (true);
	}
	return (insert_sym(symbols, symbol, infos));
}
