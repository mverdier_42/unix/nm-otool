/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 13:31:55 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/06 19:12:17 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_NM_H
# define FT_NM_H

# include "libft.h"
# include <sys/mman.h>
# include <sys/stat.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <mach-o/ranlib.h>
# include <fcntl.h>
# include <stdlib.h>
# include <ar.h>

# define AR_MAGIC	0x72613c21
# define AR_CIGAM	0x213c6172

# define OPT_NONE	0x0000000
# define OPT_G		0x0000001
# define OPT_N		0x0000010
# define OPT_P		0x0000100
# define OPT_R		0x0001000
# define OPT_ULOW	0x0010000
# define OPT_UUP	0x0100000
# define OPT_J		0x1000000

typedef struct			s_opt
{
	int						val;
	char					c;
}						t_opt;

typedef struct			s_cpu
{
	cpu_type_t				type;
	char					*name;
}						t_cpu;

typedef struct			s_seg
{
	uint8_t					nsect;
	uint8_t					count;
}						t_seg;

typedef struct			s_symbol
{
	long long int			value;
	char					symbol;
	char					*name;
	uint32_t				name_off;
	uint32_t				str_off;
	uint32_t				str_size;
	struct s_symbol			*next;
}						t_symbol;

typedef struct			s_garb32
{
	struct symtab_command	*st_cmd;
	struct nlist			*array;
	char					*tab;
	t_symbol				*symbol;
	uint32_t				i;
}						t_garb32;

typedef struct			s_garb64
{
	struct symtab_command	*st_cmd;
	struct nlist_64			*array;
	char					*tab;
	t_symbol				*symbol;
	uint32_t				i;
}						t_garb64;

typedef struct			s_header
{
	char					*name;
	uint32_t				magic;
	bool					is_64;
	bool					is_fat;
	bool					is_archive;
	bool					swap;
	off_t					size;
	int						opt;
}						t_header;

bool					ft_nm(char *file_name, int ac, t_header infos);
bool					handle_archive(void *ptr, t_header infos);
bool					err(char *msg, char *file_name);
int						parse_options(int ac, char **av, t_header *infos);

bool					get_header(void *ptr, t_header *file_infos, off_t size,
						char *file_name);
bool					magic_is_64(uint32_t magic);
bool					magic_is_fat(uint32_t magic);
bool					magic_should_swap(uint32_t magic);
bool					is_valid_file(uint32_t type);
bool					is_archive(uint32_t magic);

bool					dump_fat_header(void *ptr, t_header file_infos, int ac);
t_symbol				*dump_header32(void *ptr, t_header infos);
t_symbol				*dump_header64(void *ptr, t_header infos);

uint32_t				swap_uint32(uint32_t val);
uint64_t				swap_uint64(uint64_t val);
uint32_t				swap(uint32_t val, t_header infos);
uint64_t				swap64(uint64_t val, t_header infos);
void					swap_fat_header(struct fat_header *header);
void					swap_fat_arch(struct fat_arch *arch);

struct fat_header		fill_fat_header(void *ptr, t_header file);
struct fat_arch			fill_fat_arch(void *ptr, t_header file, off_t offset);
struct mach_header		fill_header32(void *ptr, t_header file);
struct mach_header_64	fill_header64(void *ptr, t_header file);

bool					get_symbol32(void *ptr, struct load_command *lc,
						t_header infos, t_symbol **symbols);
bool					get_symbol64(void *ptr, struct load_command *lc,
						t_header infos, t_symbol **symbols);
char					get_symcase(uint8_t type, char c);
char					get_sym_segment64(void *ptr, t_header infos,
						uint8_t nsect);
char					get_sym_segment32(void *ptr, t_header infos,
						uint8_t nsect);

bool					insert_symbol(t_symbol **symbols, t_symbol *symbol,
						t_header infos);
bool					print_symbols64(t_symbol *symbols, int ac,
						t_header infos);
bool					print_symbols32(t_symbol *symbols, int ac,
						t_header infos);
void					put_nm_str(char *str, t_symbol *sym, t_header infos);
void					delete_symbols(t_symbol *symbols);

#endif
