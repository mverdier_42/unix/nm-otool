/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_otool.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 13:31:55 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/06 15:10:59 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_OTOOL_H
# define FT_OTOOL_H

# include "libft.h"
# include <sys/mman.h>
# include <sys/stat.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <mach-o/ranlib.h>
# include <fcntl.h>
# include <stdlib.h>
# include <ar.h>

# define AR_MAGIC	0x72613c21
# define AR_CIGAM	0x213c6172

typedef struct			s_cpu
{
	cpu_type_t				type;
	char					*name;
}						t_cpu;

typedef struct			s_sect32
{
	uint32_t				addr;
	uint32_t				size;
	uint32_t				off;
	uint32_t				align;
}						t_sect32;

typedef struct			s_sect64
{
	uint64_t				addr;
	uint64_t				size;
	uint32_t				off;
	uint32_t				align;
}						t_sect64;

typedef struct			s_header
{
	char					*name;
	uint32_t				magic;
	bool					is_64;
	bool					is_fat;
	bool					is_archive;
	bool					is_from_archive;
	bool					swap;
	off_t					size;
}						t_header;

bool					ft_otool(char *file_name);
bool					handle_archive(void *ptr, t_header infos);
bool					err(char *msg, char *file_name);
int						err2(char *msg, char *file_name);

bool					get_header(void *ptr, t_header *file_infos, off_t size,
						char *file_name);
bool					magic_is_64(uint32_t magic);
bool					magic_is_fat(uint32_t magic);
bool					magic_should_swap(uint32_t magic);
bool					is_valid_file(uint32_t type);
bool					is_archive(uint32_t magic);

bool					dump_fat_header(void *ptr, t_header file_infos);
bool					dump_header32(void *ptr, t_header infos);
bool					dump_header64(void *ptr, t_header infos);
bool					get_segment32(void *ptr, t_header infos, t_sect32 *sect,
						struct mach_header header);
bool					get_segment64(void *ptr, t_header infos, t_sect64 *sect,
						struct mach_header_64 header);

uint32_t				swap_uint32(uint32_t val);
uint64_t				swap_uint64(uint64_t val);
uint32_t				swap(uint32_t val, t_header infos);
uint64_t				swap64(uint64_t val, t_header infos);
void					swap_fat_header(struct fat_header *header);
void					swap_fat_arch(struct fat_arch *arch);

struct fat_header		fill_fat_header(void *ptr, t_header file);
struct fat_arch			fill_fat_arch(void *ptr, t_header file, off_t offset,
						t_header *infos);
struct mach_header		fill_header32(void *ptr, t_header file);
struct mach_header_64	fill_header64(void *ptr, t_header file);

#endif
