/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_otool.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 17:00:33 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/06 19:40:02 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

static int	init_error(char *msg, char *file_name)
{
	ft_dprintf(2, msg, file_name);
	return (-1);
}

static int	init_otool(char *file_name, struct stat *buf, void **ptr)
{
	int		fd;

	if ((fd = open(file_name, O_RDONLY)) < 0)
		return (init_error("Cannot open %s.\n", file_name));
	if (fstat(fd, buf) < 0)
		return (init_error("Cannot retrieve infomation from %s.\n", file_name));
	if ((buf->st_mode & S_IFMT) != S_IFREG &&
		(buf->st_mode & S_IFMT) != S_IFLNK &&
		(buf->st_mode & S_IFMT) != S_IFSOCK)
		return (init_error("%s: not a valid file.\n", file_name));
	if (buf->st_size <= 0)
	{
		ft_printf("%s: is not an object file\n", file_name);
		return (-1);
	}
	if ((*ptr = mmap(0, buf->st_size, PROT_READ, MAP_PRIVATE, fd, 0))
		== MAP_FAILED)
		return (init_error("%s: mmap error.\n", file_name));
	close(fd);
	return (fd);
}

bool		ft_otool(char *file_name)
{
	int			fd;
	void		*ptr;
	struct stat	buf;
	t_header	infos;

	ptr = NULL;
	if ((fd = init_otool(file_name, &buf, &ptr)) < 0 ||
		!get_header(ptr, &infos, buf.st_size, file_name))
		return (false);
	if (infos.is_archive)
		handle_archive(ptr, infos);
	else if (infos.is_fat)
		dump_fat_header(ptr, infos);
	else if (infos.is_64)
		dump_header64(ptr, infos);
	else
		dump_header32(ptr, infos);
	if (munmap(ptr, buf.st_size) < 0)
		return (err("%s: munmap error.\n", file_name));
	return (true);
}
