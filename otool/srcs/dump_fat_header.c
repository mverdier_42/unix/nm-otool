/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dump_fat_header.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 17:27:16 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/05 19:00:37 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

static void	print_arch(struct fat_arch arch, char *file_name, uint32_t nfat)
{
	int				i;
	int				size;
	static t_cpu	cpus[] = { {CPU_TYPE_X86_64, "x86_64"},
		{CPU_TYPE_I386, "i386"}, {CPU_TYPE_I860, "i860"},
		{CPU_TYPE_POWERPC, "ppc"}, {CPU_TYPE_POWERPC64, "ppc64"},
		{CPU_TYPE_ARM, "arm"}, {CPU_TYPE_ARM64, "arm64"},
		{CPU_TYPE_MC680x0, "mc680x0"}, {CPU_TYPE_MC88000, "mc88000"},
		{CPU_TYPE_MC98000, "mc98000"}, {CPU_TYPE_VAX, "vax"},
		{CPU_TYPE_HPPA, "hppa"}, {CPU_TYPE_SPARC, "sparc"} };

	size = sizeof(cpus) / sizeof(t_cpu);
	i = 0;
	while (i < size)
	{
		if (cpus[i].type == arch.cputype)
		{
			if (nfat > 1)
				ft_printf("%s (architecture %s):\n", file_name, cpus[i].name);
			else
				ft_printf("%s:\n", file_name);
			return ;
		}
		i++;
	}
}

static bool	dump_all_archs(void *ptr, t_header infos)
{
	struct fat_header	header;
	struct fat_arch		arch;
	size_t				arch_offset;
	t_header			arch_infos;

	if ((header = fill_fat_header(ptr, infos)).magic == 0)
		return (false);
	arch_offset = sizeof(struct fat_header);
	while ((arch_offset - sizeof(header)) / sizeof(arch) < header.nfat_arch)
	{
		arch = fill_fat_arch(ptr, infos, arch_offset, &arch_infos);
		if (arch.offset == 0 || !get_header(ptr + arch.offset, &arch_infos,
			infos.size - arch.offset, infos.name))
			return (false);
		arch_infos.is_fat = true;
		print_arch(arch, infos.name, header.nfat_arch);
		if (arch_infos.is_archive)
			handle_archive(ptr + arch.offset, arch_infos);
		else if (arch_infos.is_64)
			dump_header64(ptr + arch.offset, arch_infos);
		else
			dump_header32(ptr + arch.offset, arch_infos);
		arch_offset += sizeof(struct fat_arch);
	}
	return (true);
}

bool		dump_fat_header(void *ptr, t_header infos)
{
	struct fat_header	header;
	struct fat_arch		arch;
	off_t				arch_offset;
	t_header			arch_infos;

	if ((header = fill_fat_header(ptr, infos)).magic == 0)
		return (false);
	arch_offset = sizeof(struct fat_header);
	while ((arch_offset - sizeof(header)) / sizeof(arch) < header.nfat_arch)
	{
		arch = fill_fat_arch(ptr, infos, arch_offset, &arch_infos);
		if (arch.offset == 0 || !get_header(ptr + arch.offset, &arch_infos,
			infos.size - arch.offset, infos.name))
			return (false);
		arch_infos.is_fat = false;
		if (arch.cputype == CPU_TYPE_X86_64)
		{
			if (arch_infos.is_archive)
				return (handle_archive(ptr + arch.offset, arch_infos));
			return (dump_header64(ptr + arch.offset, arch_infos));
		}
		arch_offset += sizeof(struct fat_arch);
	}
	return (dump_all_archs(ptr, infos));
}
