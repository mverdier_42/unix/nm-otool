/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 13:58:55 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/07 16:23:04 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

int		main(int ac, char **av)
{
	int			i;

	if (ac < 2)
	{
		ft_dprintf(2, "error: %s: at least one file must be specified\n",
			av[0]);
		return (0);
	}
	i = 1;
	while (i < ac)
	{
		if (!ft_otool(av[i]))
			return (0);
		i++;
	}
	return (0);
}
