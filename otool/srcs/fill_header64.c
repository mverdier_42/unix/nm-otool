/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_header64.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/04 19:52:04 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/05 18:24:18 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

struct mach_header_64	fill_header64(void *ptr, t_header file)
{
	struct mach_header_64	*src;
	struct mach_header_64	des;

	if ((size_t)file.size < sizeof(struct mach_header_64))
	{
		des.magic = 0;
		ft_printf("%s: is not an object file\n", file.name);
		return (des);
	}
	src = (struct mach_header_64*)ptr;
	des.magic = swap(src->magic, file);
	des.filetype = swap(src->filetype, file);
	des.ncmds = swap(src->ncmds, file);
	des.sizeofcmds = swap(src->sizeofcmds, file);
	des.flags = swap(src->flags, file);
	return (des);
}
