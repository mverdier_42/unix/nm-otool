/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_fat_header.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 16:45:53 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/05 18:22:25 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

struct fat_header	fill_fat_header(void *ptr, t_header file)
{
	struct fat_header	*src;
	struct fat_header	des;

	if ((size_t)file.size < sizeof(struct fat_header))
	{
		des.magic = 0;
		ft_printf("%s: is not an object file\n", file.name);
		return (des);
	}
	src = (struct fat_header*)ptr;
	des.magic = swap(src->magic, file);
	des.nfat_arch = swap(src->nfat_arch, file);
	return (des);
}
