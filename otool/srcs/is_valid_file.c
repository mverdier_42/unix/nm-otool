/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_valid_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 17:03:49 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/21 15:29:02 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

bool	is_valid_file(uint32_t type)
{
	return (type == MH_OBJECT || type == MH_EXECUTE || type == MH_FVMLIB ||
		type == MH_CORE || type == MH_PRELOAD || type == MH_DYLIB ||
		type == MH_DYLINKER || type == MH_BUNDLE || type == MH_DYLIB_STUB ||
		type == MH_DSYM || type == MH_KEXT_BUNDLE);
}
