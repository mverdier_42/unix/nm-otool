/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_segment32.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 13:05:46 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/05 13:07:04 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

static int	fill_section(struct section *section, t_sect32 *sect,
	t_header infos)
{
	uint32_t	align;
	uint32_t	i;

	if (!ft_strcmp(section->sectname, "__text"))
	{
		sect->addr = swap(section->addr, infos);
		sect->size = swap(section->size, infos);
		sect->off = swap(section->offset, infos);
		if (sect->off + sect->size > (uint32_t)infos.size)
		{
			ft_dprintf(2, "%s: section offset + size extends from file size\n",
				infos.name);
			return (-1);
		}
		align = 2;
		i = 1;
		while (i++ < swap(section->align, infos))
			align *= 2;
		sect->align = align;
		return (1);
	}
	return (0);
}

static int	get_sect(void *ptr, struct segment_command *sc, t_sect32 *sect,
	t_header infos)
{
	struct section	*section;
	uint32_t		i;
	int				res;

	if ((size_t)((void*)sc - ptr) + sizeof(*sc) + sizeof(struct section) >
		(size_t)infos.size)
		return (err2("%s: unaccessible data (segment_command)\n", infos.name));
	section = (struct section*)((void*)sc + sizeof(*sc));
	i = 0;
	while (i++ < swap(sc->nsects, infos))
	{
		if ((res = fill_section(section, sect, infos)) == 1 || res == -1)
			return (res);
		if ((size_t)((void*)section - ptr) + 2 * sizeof(*section) >
			(size_t)infos.size)
			return (err2("%s: unaccessible data (section)\n", infos.name));
		section = (void*)section + sizeof(*section);
	}
	return (0);
}

static int	get_seg_text32(void *ptr, struct load_command *lc, t_sect32 *sect,
	t_header infos)
{
	struct segment_command	*sc;

	if ((size_t)((void*)lc - ptr) + sizeof(*sc) > (size_t)infos.size)
		return (err2("%s: unaccessible data (segment_command)\n", infos.name));
	sc = (struct segment_command*)lc;
	if (!ft_strcmp(sc->segname, "__TEXT") || !ft_strcmp(sc->segname, ""))
	{
		if (swap(sc->filesize, infos) > swap(sc->vmsize, infos))
		{
			return (err2("%s: vmsize differ from filesize for load command\n",
				infos.name));
		}
		if (swap(sc->fileoff, infos) + swap(sc->filesize, infos) >
			(uint64_t)infos.size)
		{
			return (err2("%s: fileoff + filesize extends past memory\n",
				infos.name));
		}
		return (get_sect(ptr, sc, sect, infos));
	}
	return (0);
}

bool		get_segment32(void *ptr, t_header infos, t_sect32 *sect,
	struct mach_header header)
{
	struct load_command		*lc;
	uint32_t				i;
	int						res;

	i = 0;
	if (infos.size - sizeof(header) < sizeof(*lc))
		return (err("%s: unaccessible data (load_command)\n", infos.name));
	lc = ptr + sizeof(header);
	while (i < header.ncmds)
	{
		if (swap(lc->cmd, infos) == LC_SEGMENT)
		{
			if ((res = get_seg_text32(ptr, lc, sect, infos)) < 0)
				return (false);
			else if (res == 1)
				return (true);
		}
		if ((size_t)((void*)lc - ptr) + swap(lc->cmdsize, infos) + sizeof(*lc) >
			(size_t)infos.size)
			return (err("%s: unaccessible data (load_command)\n", infos.name));
		lc = (void*)lc + swap(lc->cmdsize, infos);
		i++;
	}
	return (true);
}
