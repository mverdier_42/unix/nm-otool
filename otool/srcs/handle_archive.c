/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_archive.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 15:08:55 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/04 17:24:49 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

static void	dump(void *ptr, t_header *obj)
{
	obj->is_from_archive = true;
	if (obj->is_fat)
		dump_fat_header(ptr, *obj);
	else if (obj->is_64)
		dump_header64(ptr, *obj);
	else
		dump_header32(ptr, *obj);
}

bool		handle_archive(void *ptr, t_header infos)
{
	size_t			offset;
	size_t			len;
	struct ar_hdr	*header;
	char			*str;
	t_header		obj;

	header = (struct ar_hdr*)(ptr + SARMAG);
	offset = SARMAG + sizeof(*header) + ft_atoi(header->ar_size);
	ft_printf("Archive : %s\n", infos.name);
	while ((off_t)offset < infos.size)
	{
		if (ft_atoi((header = (struct ar_hdr*)(ptr + offset))->ar_size) <= 0)
			return (err("%s: bad archive size\n", infos.name));
		str = ptr + offset + sizeof(*header);
		len = offset + sizeof(*header) + ft_atoi(header->ar_name + 3);
		if (offset + sizeof(*header) + ft_atoi(header->ar_size) >
			(size_t)infos.size)
			return (err("%s: archive extends past the file\n", infos.name));
		ft_printf("%s(%s):\n", infos.name, str);
		if (!get_header(ptr + len, &obj, infos.size, str))
			return (false);
		dump(ptr + len, &obj);
		offset += sizeof(*header) + ft_atoi(header->ar_size);
	}
	return (true);
}
