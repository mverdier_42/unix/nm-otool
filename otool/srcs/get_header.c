/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_header.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 17:05:29 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/05 18:40:33 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

bool	get_header(void *ptr, t_header *infos, off_t size, char *file_name)
{
	if (size < (off_t)sizeof(uint32_t))
		return (err("%s: is not an object file\n", infos->name));
	infos->magic = *(uint32_t*)ptr;
	infos->is_64 = magic_is_64(infos->magic);
	infos->is_fat = magic_is_fat(infos->magic);
	infos->is_archive = is_archive(infos->magic);
	infos->swap = magic_should_swap(infos->magic);
	infos->size = size;
	infos->name = file_name;
	return (true);
}
