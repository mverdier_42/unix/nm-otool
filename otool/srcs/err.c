/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   err.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:25:32 by mverdier          #+#    #+#             */
/*   Updated: 2018/11/30 16:44:14 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

int		err2(char *msg, char *file_name)
{
	ft_dprintf(2, msg, file_name);
	return (-1);
}

bool	err(char *msg, char *file_name)
{
	ft_dprintf(2, msg, file_name);
	return (false);
}
