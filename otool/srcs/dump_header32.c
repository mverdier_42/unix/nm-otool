/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dump_header32.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 17:37:31 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/05 16:03:59 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_otool.h"

static void	print_mem(uint8_t *mem, t_sect32 sect, t_header infos)
{
	uint32_t			size;
	uint32_t			i;

	ft_printf("Contents of (__TEXT,__text) section\n");
	size = 0;
	while (size < sect.size)
	{
		ft_printf("%.8llx\t", sect.addr);
		i = 0;
		while (i < 16 && i < sect.size - size)
		{
			if (!infos.swap || (i + 1) % 4 == 0)
				ft_printf("%.2x ", *(mem++));
			else
				ft_printf("%.2x", *(mem++));
			i++;
		}
		ft_putchar('\n');
		sect.addr += 16;
		size += 16;
	}
}

bool		dump_header32(void *ptr, t_header infos)
{
	struct mach_header	header;
	t_sect32			sect;

	header = fill_header32(ptr, infos);
	if (header.magic == 0)
		return (false);
	if (!is_valid_file(header.filetype))
	{
		ft_printf("%s: is not an object file\n", infos.name);
		return (false);
	}
	if (!get_segment32(ptr, infos, &sect, header))
		return (false);
	if (!infos.is_fat && !infos.is_from_archive)
		ft_printf("%s:\n", infos.name);
	print_mem(ptr + sect.off, sect, infos);
	return (true);
}
